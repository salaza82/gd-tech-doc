\documentclass{article}

\usepackage{arxiv}
\usepackage{macro_journals}

\usepackage{bm}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{url}
\usepackage{float}
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{microtype}      % microtypography
\usepackage{tabularx,caption,booktabs}
\usepackage{afterpage}
\usepackage{xspace}
\usepackage{mathrsfs}
\usepackage{nicefrac}
\usepackage{microtype}
\usepackage{gensymb}
\usepackage{pdflscape}
\usepackage{units}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{arydshln}

\usepackage{lineno}
\linenumbers

\usepackage[capitalize]{cleveref}
\crefformat{section}{Sec.~#2#1#3}
\Crefformat{section}{Section~#2#1#3}
\crefformat{appendix}{App.~#2#1#3}
\Crefformat{appendix}{Appendix~#2#1#3}
\crefformat{table}{Tab.~#2#1#3}
\Crefformat{table}{Table~#2#1#3}
\crefformat{pluralequation}{#2\black{Eqs.~(}#1\black{)}#3}
\Crefformat{pluralequation}{#2\black{Equations~(}#1\black{)}#3}
\crefformat{pluralfigure}{#2\black{Figs.~}#1#3}
\Crefformat{pluralfigure}{#2\black{Figures~}#1#3}

\def\J {\ensuremath{J}\xspace}
\def\GS {\ensuremath{\mathcal{GS}}\xspace}
\def\B {\ensuremath{\mathcal{B}}\xspace}
\newcommand{\sv}{\langle \sigma \mathit{v} \rangle}

\newcommand\CellTop{\rule{0pt}{2.35ex}}
\newcommand\CellTopTwo{\rule{0pt}{2.8ex}}
\newcommand\CellTopThree{\rule{0pt}{3.2ex}}
\newcommand\CellTopFour{\rule{0pt}{3.6ex}}

\title{%
Combined Dark Matter Searches Towards Dwarf Spheroidal Galaxies with Fermi-LAT, HAWC, H.E.S.S., MAGIC, and VERITAS \\
(The Glory Duck Project)}
\author{Daniel Salazar-Gallegos}

\begin{document}
\maketitle
\section*{Fast Analysis Facts}
\begin{itemize}
    \item Data
\end{itemize}

\tableofcontents
\section{Introduction}

In this analysis, we develop further HAWC's previous dark matter (DM) toward dwarf
spheroidal (dSph) galaxies \cite{Albert_2018}. For the first time, we perform a
combination analysis towards dwarf spheroidal galaxies including data from five
$\gamma$-ray observatories: Fermi-LAT, H.E.S.S., HAWC, MAGIC, and VERITAS. HAWC's
analysis of the dSphs resembles \cite{Albert_2018} except we treat our dSph as
extended. In order to viably combine our results we use the same source extension
and annihilation spectra across all 5 participating experiments. \\

This combination analysis looks specifically for DM annihilation to the channels\\
(make a table?)\\
with $\gamma$-ray final state products. From (table cite), HAWC contributions to
the dataset are bolded. This analysis uses spectral models provided by the
\emph{Poor Particle Physicist's Cookbook} \cite{Cirelli_2011} using their
electroweak corrected spectra for DM annihilation. Our shared J-Factor models
are using Geringer-Sameth's annihilation profiles \cite{Geringer_Sameth_2015}.\\

The purpose of the combination is to increase the statistical power of each
experiment. Together we can and do produce the strictest annihilation limits on DM
for dSph sources. Presented in this tech doc are details only relevant for internal
HAWC reproduction of the study. This study could stand alone as a refresh of dSph DM
limits with more data with extended source models.

\section{Data and Background}
\subsection{Data Set Chosen}
The maps used for this analysis contain ??1038?? days of data and use (pass ??)
reconstruction. The analysis is performed using the f-hit energy binning scheme
similar to what was done for the Crab.\cite{Abeysekara_2017}\

\subsection{Background Estimation}
This analysis was done of dSph galaxies because of their marked difference in
gravitation to luminous baryonic matter. The dSphs are also small in HAWC's field of
view that we are not sensitive to large or small scale anisotropies. Therefor we make
no additional assumptions of the background incident from our sources. We consider
only the additional events from HAWC's systematics encoded in our detector
resolution. Questions arise when considering other processes that can occur to
annihilation products in transit to HAWC. These are addressed in Section 3.

\subsection{Software Tools and Development}
This analysis was performed using HAL, 3ML, in Py2. I developed a source model to
implement the PPPC into HAWC software. This model and corresponding Monte Carlo
simulation was consolidated into a dictionary for other collaborators. A version of
this dictionary was made for both Py2 and Py3.\\

The analysis was performed using the $f_{\textrm{hit}}$ framework performed in
the Crab paper\cite{Abeysekara_2017}. The PPPC model selected for this analysis
included electroweak corections. However, the software was constructed to be flexible
with both. Each no/EW model has it's corresponding dictionary file:
\begin{itemize}
    \item \texttt{dmCirSpecDict.npy}: Electro-Weak corrected PPPC $\chi\chi \rightarrow XX$ annihilation spectra for generic SM particle X.
    \item \texttt{dmCirSpecDictNoEW.npy}: Not EW-corrected PPPC $\chi\chi \rightarrow XX$ annihilation spectra for generic SM particle X.
\end{itemize}
These files can also be used for decay channels. The PPPC describes how. \cite{Cirelli_2011}.
A better naming scheme is welcomed especially as Py3 compatible versions of these
dictionaries is implemented. Numpy's 'pickle' is not backwards compatible, so Numpy
versions using Py3 are not able to opened the currently available dictionaries.

\section{Analysis Technique}

\subsection{Monte Carlo Simulation}
The expected differential photon flux from a DM-DM annihilation to standard model
particles over solid angle is described by the familiar equation.
$$
\frac{d\Phi}{dE} = \frac{J\langle \sigma v \rangle}{8\pi M^{2}_{\chi}}
\frac{dN(M_\chi, \textrm{SM Channel})}{dE}.
$$
Above, $\langle \sigma v\rangle$ is the velocity weighted annihilation cross-section.
$J$ is the astrophysical J-factor and is defined as
$$
J = \int \int \rho^2_{\chi}(l, \Omega) dl d\Omega
$$
and $\frac{dN}{dE}$ is the differential average number of photons produced at each
energy per annihilation. For this value, we import the PPPC with electro-weak
corrections \cite{Cirelli_2011}. The spectrum is implemented as a model script
in 3ML. The J-factor profiles for each source is imported from Geringer-Sameth
\cite{Geringer_Sameth_2015}. We create PSIDE $3.5\times 10^{-3}$ maps of the
J-factors for each source are integrated over every spatial bin and passed to the
fitting software.

\subsection{Source Selection}
We use many of the dSph presented in our previous dSph DM search \cite{Albert_2018}.
Our sources include Bootes I, Coma Berenices, Canes Venatici I + II, Draco, Hercules,
Leo I, II, + IV, Segue 1, Sextans, and Ursa Major I + II. Triangulum was excluded
from the Glory Duck analysis because of large uncertainties in its J-factor.
Ursa Minor was also excluded from HAWC's contribution to the combination because
the source extension model extended Ursa Minor beyond HAWC's field of view. Ursa
Minor was not expected to contribute significantly to the combined limit, so work
was not invested in a solution to include Ursa Minor.

The DM annihilation channels probed for the Glory Duck combination include $b\bar{b}
, e\bar{e}, \gamma\gamma, \mu\bar{\mu}, \tau\bar{\tau}, t\bar{t}, W^+W^-,
 \textrm{and }ZZ$. HAWC does not provide to Glory Duck for the $\gamma\gamma$ channel
because results were not published one this channel only for HAWC at the time of
this analysis' combination.

\begin{table}[t]
\captionsetup{font=small}
\centering
\caption{Summary of the relevant properties of the dSphs used in the present work. Column 1 lists the dSphs. Columns 2 and 3 present their heliocentric distance and galactic coordinates, respectively. Columns 4 and 5 report the \J-factors of each source given from the \GS and \B independent studies and their estimated $\pm 1\sigma$ uncertainties. The values $\log_{10}J$~(\GS set) correspond to the mean \J-factor values  for a source extension truncated at the outermost observed star. The values $\log_{10}J$~(\B set)  are provided for a source extension at the tidal radius of each dSph.}
% Vincent P. wrote a script to extract the J factor and its uncertainties from the GS and B file: see https://hess-confluence.desy.de/confluence/display/HESSshared/J+factors
\small{\begin{tabular}{ccccc}
% \begin{tabular}{p{1.5cm} p{1.5cm} p{1.5cm} p{1.5cm} p{1.7cm} p{1.5cm} p{1.5cm}}
\hline
\hline
\CellTopTwo{}
Name & Distance & $l, b$ & $\log_{10}J$~(\GS set) & $\log_{10}J$~(\B set)\\
& \scriptsize{(kpc)} &  \scriptsize{($\degree$)} & \scriptsize{$\log_{10}(\rm{GeV}^2 \rm{cm}^{-5}\rm{sr})$} & \scriptsize{$\log_{10}(\rm{GeV}^2 \rm{cm}^{-5}\rm{sr})$}  \\
\hline
\CellTopTwo{}
Bo\"otes I & $66$ & $358.08,\: 69.62$ & $18.24^{+0.40}_{-0.37}$ & $18.85^{+1.10}_{-0.61}$  \\
\CellTopTwo{}
Canes Venatici I & $218$ & $74.31,\: 79.82$ & $17.44^{+0.37}_{-0.28}$ & $17.63^{+0.50}_{-0.20}$  \\
\CellTopTwo{}
Canes Venatici II & $160$ & $113.58,\: 82.70$ & $17.65^{+0.45}_{-0.43}$ & $18.67^{+1.54}_{-0.97}$  \\
\CellTopTwo{}
Carina & $105$ & $260.11,\: -22.22$ & $17.92^{+0.19}_{-0.11}$ & $18.02^{+0.36}_{-0.15}$ \\
\CellTopTwo{}
Coma Berenices & $44$ & $241.89,\: 83.61$ & $19.02^{+0.37}_{-0.41}$ & $20.13^{+1.56}_{-1.08}$  \\
\CellTopTwo{}
Draco & $76$ & $86.37,\: 34.72$ & $19.05^{+0.22}_{-0.21}$ &  $19.42^{+0.92}_{-0.47}$  \\
\CellTopTwo{}
Fornax & $147$ & $237.10,\: -65.65$ & $17.84^{+0.11}_{-0.06}$ &  $17.85^{+0.11}_{-0.08}$  \\
\CellTopTwo{}
Hercules & $132$ & $28.73,\: 36.87$ & $16.86^{+0.74}_{-0.68}$ &  $17.70^{+1.08}_{-0.73}$  \\
\CellTopTwo{}
Leo I & $254$ & $225.99,\: 49.11$ & $17.84^{+0.20}_{-0.16}$ &  $17.93^{+0.65}_{-0.25}$  \\
\CellTopTwo{}
Leo II & $233$ & $220.17,\: 67.23$ & $17.97^{+0.20}_{-0.18}$ &  $18.11^{+0.71}_{-0.25}$  \\
\CellTopTwo{}
Leo IV & $154$ & $265.44,\: 56.51$ & $16.32^{+1.06}_{-1.70}$ & $16.36^{+1.44}_{-1.65}$  \\
\CellTopTwo{}
Leo V & $178$ & $261.86,\: 58.54$ & $16.37^{+0.94}_{-0.87}$ & $16.30^{+1.33}_{-1.16}$  \\
\CellTopTwo{}
Leo T & $417$ & $214.85,\: 43.66$ & $17.11^{+0.44}_{-0.39}$ & $17.67^{+1.01}_{-0.56}$  \\
\CellTopTwo{}
Sculptor & $86$ & $287.53,\: -83.16$ & $18.57^{+0.07}_{-0.05}$ &  $18.63^{+0.14}_{-0.08}$  \\
\CellTopTwo{}
Segue I & $23$ & $220.48,\: 50.43$ & $19.36^{+0.32}_{-0.35}$ &  $17.52^{+2.54}_{-2.65}$  \\
\CellTopTwo{}
Segue II & $35$ & $149.43,\: -38.14$ & $16.21^{+1.06}_{-0.98}$ & $19.50^{+1.82}_{-1.48}$  \\
\CellTopTwo{}
Sextans & $86$ & $243.50,\: 42.27$ & $17.92^{+0.35}_{-0.29}$ &  $18.04^{+0.50}_{-0.28}$  \\
\CellTopTwo{}
Ursa Major I & $97$ & $159.43,\: 54.41$ & $17.87^{+0.56}_{-0.33}$ & $18.84^{+0.97}_{-0.43}$  \\
\CellTopTwo{}
Ursa Major II & $32$ & $152.46,\: 37.44$ & $19.42^{+0.44}_{-0.42}$ & $20.60^{+1.46}_{-0.95}$  \\
\CellTopTwo{}
Ursa Minor & $76$ & $104.97,\: 44.80$ & $18.95^{+0.26}_{-0.18}$ & $19.08^{+0.21}_{-0.13}$  \\
\hline
\hline
\CellTopTwo{}
\end{tabular}}
\label{tab:J_factor}
\end{table}

\subsection{Likelihood Methods}
We perform a HAWC dark matter standard binned maximum likelihood analysis. The
maximum likelihood is used also for a likelihood ratio test. This test lets us
calculate the significance of each source with a low signal-to-noise ratio. We
reproduce the previous dSph analysis except the sources are treated as extended.
For the vast majority of our sources, this extension is no greater than 2 degrees.
We calculate the likelihood of each source and model, assuming events are Poisson
distributed, with

%
\begin{equation}
    \label{Eq:likelihood_profile}
\lambda \left( \sv \mid \bm{\mathcal{D}_{\text{dSphs}}} \right) = \frac{\mathcal{L} \left( \sv ; \bm{\hat{\hat{\nu}}} \mid \bm{\mathcal{D}_{\text{dSphs}}} \right)}{\mathcal{L} \left( \widehat{\sv} ; \bm{\hat{\nu}} \mid \bm{\mathcal{D}_{\text{dSphs}}} \right)} ,
\end{equation}
%

$$
\mathcal{L}=\prod_i \frac{(B_i+S_i)^{N_i}e^{-(B_i+S_i)}}{N_i !}.
$$
Where $S_i$ is the sum of expected number of signal counts. $B_i$ is the number of
background counts observed. $N_i$ is the total number of counts. The \textit{i}th
bin is iterated over spatial and $f_{\textrm{hit}}$. With the likelihoods we perform
a ratio test
$$
\textrm{TS} = -2\ln\left(\frac{\mathcal{L}}{\mathcal{L}^{\textrm{max}}}\right).
$$
$\mathcal{L}^{\textrm{max}}$ here is equivalent to $\mathcal{L}(N_i, B_i,S_i=0)$ or
no signal counts.
\begin{itemize}
    \item I use the standard DM spectrum eqn: (some factor) * dN/dE * Jfac
    \item dN/dE produced from the PPPC and includes EW corrections
    \begin{itemize}
        \item analysis for 7channels, 13 dwarves
    \end{itemize}
    \item Spatial j-factor from Garenger-Sameth. This model uses extended source
    Models
    \begin{itemize}
        \item Exclude Ursa Minor because it extends beyond HAWC FOV and the limits
        from it are too much trouble for what they produce
        \item Scaling was done for comparisonto other J-facs values from ???
    \end{itemize}
    \item Combination is at LLH level. We internally produce new combinations w/HAWC
    Sources
    \begin{itemize}
        \item Then combine across the 5 other experiments. Share between us a table
        of test statistics
    \end{itemize}
    \item LL Calculation and limit calculation (didnt find any DM) :(
\end{itemize}

\begin{figure}
\centering{
\includegraphics[width=0.7\textwidth]{plots/comparison/Combined_exp_technique_2000GeV_exp1234.pdf}}
\caption{Illustration of the combination technique showing a comparison of individual $-2\ln  \lambda$ of four dSphs and their sum. The black dotted lines shows a more conservative value of the upper limit on $\langle \sigma v \rangle$ than the ones derived from a single dSph (dotted green lines). We note that each profile depends on the observational conditions in which a target object was observed. The  sensitivity of a given instrument can be degraded and the upper limits less constraining if the observations are performed in non optimal conditions, {\it i.e.} high zenith angle, exposure time.}
%\emoulin{these profiles depend on the observation conditions of the chosen object. They differ from on experiment to another. Therefore the sensitivity of a given experiment can be deteriorated just because of non optimal  observation conditions, i.e., high zenith angle observations. This deserves to be commented here.}} \dsg{Could we get the plot style format to match the plots in Fig1 and 2?}
%Left - Comparison of individual $-2\ln  \lambda$, {\it i.e.} one per dSph, for one experiment to the sum of the four, {\it i.e.} the combined one. Right - Comparison of individual experiment $-2\ln  \lambda$ for a single dSph.}}
\label{fig:illustration_combination}
 \end{figure}

\section{Systematics}
\begin{itemize}
    \item Spectral:
    \begin{itemize}
        \item Inverse Compton Scattering
        \item Comparison to pt Source using the same spectral Models
    \end{itemize}
    \item Spatial:
    \begin{itemize}
        \item Jfactor Scaling
        \item Treatment of nuisance parameters (I think this is the same as the
        above)
        \item Pointing systematic.
        \begin{itemize}
            \item New Declinations
            \item Comparison of limits
        \end{itemize}
    \end{itemize}
\end{itemize}

\section{Results and Cross Checks}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{sec:results}

\begin{figure}[ht]
\centering{\includegraphics[scale=0.32]{plots/appendix/BootesI.pdf}
\includegraphics[scale=0.32]{plots/appendix/CanesVenaticiI.pdf}
\includegraphics[scale=0.32]{plots/appendix/CanesVenaticiII.pdf}
\includegraphics[scale=0.32]{plots/appendix/Carina.pdf}
\includegraphics[scale=0.32]{plots/appendix/ComaBerenices.pdf}
\includegraphics[scale=0.32]{plots/appendix/Draco.pdf}
\includegraphics[scale=0.32]{plots/appendix/Fornax.pdf}
\includegraphics[scale=0.32]{plots/appendix/Hercules.pdf}
\includegraphics[scale=0.32]{plots/appendix/LeoI.pdf}
\includegraphics[scale=0.32]{plots/appendix/LeoII.pdf}
}\\
\caption{Comparisons between the \J factors versus the angular radius for the computation of $J$ factors from Ref.~\cite{Geringer-Sameth:2014yza} (\GS set in \cref{tab:J_factor}) in blue and for the computation from Ref.~\cite{Bonnivard:2014kza, Bonnivard:2015xpq} (\B set in \cref{tab:J_factor}) in orange. The solid lines represent the central value of the \J factors while the shaded regions correspond to the 1$\sigma$ standard deviation.}
\label{fig:comparison_J_1}
\end{figure}

\begin{figure}[ht]
\centering{\includegraphics[scale=0.32]{plots/appendix/LeoIV.pdf}
\includegraphics[scale=0.32]{plots/appendix/LeoT.pdf}
\includegraphics[scale=0.32]{plots/appendix/LeoV.pdf}
\includegraphics[scale=0.32]{plots/appendix/Sculptor.pdf}
\includegraphics[scale=0.32]{plots/appendix/SegueI.pdf}
\includegraphics[scale=0.32]{plots/appendix/SegueII.pdf}
\includegraphics[scale=0.32]{plots/appendix/Sextans.pdf}
\includegraphics[scale=0.32]{plots/appendix/UrsaMajorI.pdf}
\includegraphics[scale=0.32]{plots/appendix/UrsaMajorII.pdf}
\includegraphics[scale=0.32]{plots/appendix/UrsaMinor.pdf}
}\\
\caption{Comparisons between the \J factors versus the angular radius for the computation of $J$ factors from Ref.~\cite{Geringer-Sameth:2014yza} (\GS set in \cref{tab:J_factor}) in blue and for the computation from Ref.~\cite{Bonnivard:2014kza, Bonnivard:2015xpq} (\B set in \cref{tab:J_factor}) in orange. The solid lines represent the central value of the \J factors while the shaded regions correspond to the 1$\sigma$ standard deviation.}
\label{fig:comparison_J_2}
\end{figure}

% Vincent
No significant DM emission was observed by any of the five telescopes.
We present upper limits on $\sv$ using the test statistics, $\mathrm{TS}$:
\begin{equation}
    \mathrm{TS} = -2 \ln{ \lambda(\sv)},
\end{equation}
where $\lambda(\sv)$ is defined in Eq.~\ref{Eq:likelihood_profile}. We determine the upper limits by solving $\mathrm{TS}= 2.71 $, where 2.71 corresponds to a one-sided 95\% confidence level assuming the test statistic behaves like a $\chi^2$ distribution with one degree of freedom.
%In consequence, we present the results in terms of upper limits on $\sv$ by solving $-2 \ln{ \lambda(\sv)} = 2.71 $ (see \cref{Eq:likelihood_profile}), where 2.71 corresponds to a one-sided 95\% confidence level assuming that the test statistics behave like a $\chi^2$ distribution with one degree of freedom.
We study seven DM self annihilation channels, namely $W^+W^-$, $Z^+Z^-$, $b\bar{b}$, $t\bar{t}$, $e^+e^-$, $\mu^+\mu^-$, $\tau^+\tau^-$, and mono-energetic $\gamma \gamma$ channel.
The 68\% and 95\% containment bands are produced from 300 Poisson realizations of the null hypothesis corresponding to each of the combined datasets. These 300 realizations are combined identically to dSph observations. The containment bands and the median are extracted from the distribution of resulting limits on the null hypothesis. These 300 realizations are obtained either by fast simulations of the OFF observations, for H.E.S.S., MAGIC, VERITAS and HAWC, or taken from real observations of empty fields of view in the case of {\it Fermi}-LAT~\cite{Ackermann:2015zua,Fermi-LAT:2016uux,DiMauro:2021qcf}.

Confidence levels are shown in \cref{fig:limits-geringer-sameth} for the \GS set of \J-factors~\cite{Geringer-Sameth:2014yza} and in \cref{fig:limits-bonnivard} for the \B set of \J-factors~\cite{Bonnivard:2014kza, Bonnivard:2015xpq}. The combined limits are presented with their 68\% and 95\% containment bands, and are expected to be close to the median limit when no signal is present. We observe agreement with the null hypothesis for all channels, within $2\sigma$ standard deviations, between the observed limits and the expectations given by the median limits. Limits obtained from each detector are also indicated in the figures, where limits for all dSphs observed by the specific instrument have been combined.

Below \textasciitilde300 GeV, the Fermi-LAT dominates the DM limits for all annihilation channels. From \textasciitilde300 GeV to \textasciitilde2 TeV, Fermi-LAT continues to dominate for the hadronic and bosonic DM channels, yet the IACTs (H.E.S.S., MAGIC, and VERITAS) and Fermi-LAT all contribute to the limit for leptonic DM channels. For DM masses between \textasciitilde2 TeV to \textasciitilde10 TeV, the IACTs dominate leptonic DM anhilation channels, whereas both the Fermi-LAT and the IACTs dominate bosonic and hadronic DM annihilation channels. From \textasciitilde10 TeV to \textasciitilde100 TeV, both the IACTs and HAWC contribute significantly to the leptonic DM limit. For hadronic and bosonic DM, the IACTs and Fermi-LAT both contribute strongly. For the mono-energetic $\gamma \gamma$ channel, only H.E.S.S., MAGIC, and VERITAS provide an upper limit.

$Fermi$-LAT's energy resolution is $\sim 0.05$ across most of the Fermi-LAT energy range which is much smaller than the energy bins used in this analysis. $Fermi$-LAT's $\gamma \gamma$ channel analysis is done using different methods, see Ref.~\cite{2013PhRvD..88h2002A, 2015PhRvD..91l2002A}, so not suitable for combining with other results considered here. Similarly, a HAWC analysis for the $\gamma \gamma$ channel requires a separate procedure than the one described here~\cite{HAWC:2019jvm}, so no HAWC data are given for the $\gamma \gamma$ channel. We note that the containment bands are shrinking at high energy for this channel, due to the fact that VERITAS bla bla.

\Cref{fig:limits-comparison} shows a comparison of combined limits between the \GS and \B sets of \J-factors.

This comparison demonstrates the magnitude of systematic uncertainties associated with the choice of the \J-factor calculation.
The \GS and \B sets present a difference in the limits for all channels of about
% (to be calculated by people having the numbers in hand)}.
This difference is explained, see  \cref{fig:comparison_J_1,fig:comparison_J_2} in Appendix, by the fact that the \B set provides higher \J factors for all dSph except for Segue~I. This pushes the range of thermal cross-section which can be excluded to higher mass.


\begin{figure}
\centering{
\begin{tabular}{cc}
\includegraphics[width=0.35\textwidth]{plots/limits/Glory_Duck_Annihilation_WW_Geringer-Sameth_Combination_bands.pdf} &
\includegraphics[width=0.35\textwidth]{plots/limits/Glory_Duck_Annihilation_ZZ_Geringer-Sameth_Combination_bands.pdf} \\
\includegraphics[width=0.35\textwidth]{plots/limits/Glory_Duck_Annihilation_bb_Geringer-Sameth_Combination_bands.pdf} &
\includegraphics[width=0.35\textwidth]{plots/limits/Glory_Duck_Annihilation_tt_Geringer-Sameth_Combination_bands.pdf} \\
\includegraphics[width=0.35\textwidth]{plots/limits/Glory_Duck_Annihilation_ee_Geringer-Sameth_Combination_bands.pdf} &
\includegraphics[width=0.35\textwidth]{plots/limits/Glory_Duck_Annihilation_mumu_Geringer-Sameth_Combination_bands.pdf} \\
 \includegraphics[width=0.35\textwidth]{plots/limits/Glory_Duck_Annihilation_tautau_Geringer-Sameth_Combination_bands.pdf} &
\includegraphics[width=0.35\textwidth]{plots/limits/Glory_Duck_Annihilation_gammagamma_Geringer-Sameth_Combination_bands.pdf} \\
\end{tabular}
}
\caption{Upper limits at 95\% confidence level on $\sv$ in function of the DM mass for eight annihilation channels, using the set of $J$ factors from Ref.~\cite{Geringer-Sameth:2014yza} (\GS set in \cref{tab:J_factor}). The black solid line represents the observed combined limit, the black dashed line is the median of the null hypothesis corresponding to the expected limit, while the green and yellow bands show the 68\% and 95\% containment bands. Combined upper limits for each individual detector are also indicated as solid, colored lines.
%\jr{do not use light green for any line, since it gets confused with the 1sigma band}.
The value of the thermal relic cross section in function of the DM mass is given as the red dotted-dashed line~\cite{Bertone_2005}.}
\label{fig:limits-geringer-sameth}
\end{figure}


\begin{figure}
\centering{
\begin{tabular}{cc}
\includegraphics[width=0.35\textwidth]{plots/limits/Glory_Duck_Annihilation_WW_Bonnivard_Combination_bands.pdf} &
\includegraphics[width=0.35\textwidth]{plots/limits/Glory_Duck_Annihilation_ZZ_Bonnivard_Combination_bands.pdf} \\
\includegraphics[width=0.35\textwidth]{plots/limits/Glory_Duck_Annihilation_bb_Bonnivard_Combination_bands.pdf} &
\includegraphics[width=0.35\textwidth]{plots/limits/Glory_Duck_Annihilation_tt_Bonnivard_Combination_bands.pdf} \\
\includegraphics[width=0.35\textwidth]{plots/limits/Glory_Duck_Annihilation_ee_Bonnivard_Combination_bands.pdf} &
\includegraphics[width=0.35\textwidth]{plots/limits/Glory_Duck_Annihilation_mumu_Bonnivard_Combination_bands.pdf} \\
\includegraphics[width=0.35\textwidth]{plots/limits/Glory_Duck_Annihilation_tautau_Bonnivard_Combination_bands.pdf} &
\includegraphics[width=0.35\textwidth]{plots/limits/Glory_Duck_Annihilation_gammagamma_Bonnivard_Combination_bands.pdf} \\
\end{tabular}
}
\caption{Same as \cref{fig:limits-geringer-sameth}, using the set of $J$ factors from Ref.~\cite{Bonnivard:2014kza, Bonnivard:2015xpq} (\B set in \cref{tab:J_factor}).}
\label{fig:limits-bonnivard}
\end{figure}



\begin{figure}
\centering{
\begin{tabular}{ccc}
\includegraphics[width=0.3\textwidth]{plots/comparison/GD_limits_WW.pdf} &
\includegraphics[width=0.3\textwidth]{plots/comparison/GD_limits_ZZ.pdf} &
\includegraphics[width=0.3\textwidth]{plots/comparison/GD_limits_bb.pdf} \\
\includegraphics[width=0.3\textwidth]{plots/comparison/GD_limits_tt.pdf} &
\includegraphics[width=0.3\textwidth]{plots/comparison/GD_limits_ee.pdf} &
\includegraphics[width=0.3\textwidth]{plots/comparison/GD_limits_mumu.pdf} \\
\includegraphics[width=0.3\textwidth]{plots/comparison/GD_limits_tautau.pdf} &
\includegraphics[width=0.3\textwidth]{plots/comparison/GD_limits_gammagamma.pdf} &
\end{tabular}
}
\caption{Comparisons of the combined limits at 95\% confidence level for each of the eight annihilation channels when using the \J factors from Ref.~\cite{Geringer-Sameth:2014yza} (\GS set in \cref{tab:J_factor}), plain lines, and the \J factor from Ref.~\cite{Bonnivard:2014kza, Bonnivard:2015xpq} (\B set in \cref{tab:J_factor}), dashed lines. The cross section given by the thermal relic is also indicated~\cite{Bertone_2005}.}
\label{fig:limits-comparison}
\end{figure}



\afterpage{\clearpage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Discussion and Conclusions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{sec:conclusions}
% Pat and Dan S

In this multi-instrument analysis, we have used observations of 20 dSphs from the gamma-ray telescopes {\it Fermi}-LAT, H.E.S.S., MAGIC, VERITAS, and HAWC to perform a collective DM search annihilation signals. The data were combined across sources and detectors to significantly increase the sensitivity of the search. We have observed no significant deviation from the null, no DM, hypothesis, so present our results in terms of upper limits on the annihilation cross section for eight potential DM annihilation channels.

{\it Fermi}-LAT brings the most stringent constraints for continuum channels below approximately 1~TeV. the remaining detectors dominate at higher energies. Overall, for multi-TeV DM mass, the combined DM constraints from all five telescopes are 2-3 times stronger than any individual telescope for multi-TeV DM.

Derived from observations of many dSphs, our results produce robust limits given the DM content of the dSphs is relatively well constrained. The obtained limits span the largest mass range of any WIMP DM search. Our combined analysis improves the sensitivity over previously published results from each detectors which produces the most stringent limits on DM annihilation from dSphs. These results are based on deep exposures of the most promising known dSphs with the currently most sensitive gamma-ray instruments. Therefore, our results constitute a legacy of a generation of gamma-ray instruments on WIMP DM searches towards dSphs. Our results will remain the reference in the field until a new generation of more sensitive gamma-ray instruments begin operations, or until new dSphs with higher \J-factors are discovered.

This analysis serves as a proof of concept for future multi-instrument and multi-messenger combination analyses. With this collaborative effort, we have managed to sample over 4 orders in magnitude in gamma-ray energies with distinct observational techniques. Determining the nature of DM continues to be an elusive and difficult problem. Larger datasets with diverse measurement techniques could be essential to tackling the DM problem. Our collaborative work could be extended in the future to include  other gamma-ray detectors such as LHAASO~\cite{Bai:2019khm}, CTA~\cite{CTAConsortium:2018tzg}, and SWGO~\cite{Abreu:2019ahw}. A future collaboration using similar techniques as the ones described in this paper could grow even beyond gamma rays. The models we used for this study include annihilation channels with neutrinos in the final state. Advanced studies could aim to merge our results with those from neutrino observatories with large data sets. Efforts are already underway to add data from the IceCube, ANTARES, and KM3Net observatories to these gamma-ray results.

The greatest success of this analysis is that it combines data from multiple telescopes to produce strong measurements and constraints on astrophysical sources. From this perspective, these methods can be applied beyond just DM searches. Almost every astrophysical study can benefit from multi-telescope, multi-wavelength gamma-ray studies. We have enabled these telescopes to study the cosmos with greater precision and detail. Many astrophysical searches can benefit from multi-instrument gamma-ray studies, for which our analysis lays the foundation.

\begin{itemize}
    \item Show HAWC limits + Brazil bands
    \item Show combined limits and Brazil bands
    \item show TS
    \item Talk breifly about GLike and x-checks
\end{itemize}

\section{Discussion}
\begin{itemize}
    \item Best limits hooray!!
    \item Didnt find DM :(
    \item Next directions
\end{itemize}

\bibliographystyle{ieeetr}
\bibliography{refs}

\end{document}